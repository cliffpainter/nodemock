const express = require('express')
const app = express()
const port = 3000

const permissionsRouter = require("./routes/permissionsRoutes");

app.use('/userResourcePermissions', permissionsRouter);

send405 = (res) =>{res.sendStatus(405);}

app.get('/', (req, res) => {
    res.redirect("https://winwholesale.atlassian.net/wiki/spaces/EA/pages/2353070321/Access+Management+System")
})

app.put('/', (req, res) => send405(res));
app.patch('/', (req, res) => send405(res));
app.post('/', (req, res) => send405(res));
app.delete('/', (req, res) => send405(res));

app.listen(port, () => {
    console.log(`User Permissions mock server listening at http://localhost:${port}`)
})
