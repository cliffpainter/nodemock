========================================================
TEST SERVER FOR SOE SERVICES CONSUMING AMS ENDPOINT
========================================================


Endpoints exposed
/userResourcePermissions/{resourceName}/users/{username}/companies/{companyNumber}
/userResourcePermissions/{resourceName}/users/{username}

Scenarios:
(1) Test JSON from docs
{resourceName} = "worker-bee"
{userName} = "jdoe"
{companyNumber} = "00123"

(2) Test JSON from docs, multiple companyResourcePermissions blocks
{resourceName} = "worker-bee"
{userName} = "jdoe"


** fault injection 

(3) Empty response
{resourceName} = "worker-bee"
{userName} = "lazybones"

(4) 503 response
{resourceName} = "worker-bee"
{userName} = "confused"
