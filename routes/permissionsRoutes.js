const express = require('express');
const permissionsRouter = express.Router();

const app = express();

const getStdSinglePermissions = function(f) {
    console.log("Entering permissionRoutes::getPermissions");

    var theJson =
        '{' +
        '"meta": {' +
        '    "success": true,' +
        '    "message": "Resource permissions retrieved successfully."' +
        '},' +
        '"data": {' +
        '    "username": "someuser",' +
        '    "resource": "purchasing-manager",' +
        '    "globalResourcePermissions": [],' +
        '    "companyResourcePermissions": [' +
        '        {' +
        '            "companyNumber": "00110",' +
        '            "permissions": [' +
        '                "purchasing.template.create",' +
        '                "purchasing.template.delete",' +
        '                "purchasing.template.read"' +
        '            ]' +
        '        }' +
        '    ]' +
        '}';

    f(theJson);
}

const getStdMultiPermissions = function(f) {
    console.log("Entering permissionRoutes::getPermissions");

    var theJson =
        '{' +
        '"meta": {' +
        '    "success": true,' +
        '    "message": "Resource permissions retrieved successfully."' +
        '},' +
        '"data": {' +
        '    "username": "someuser",' +
        '    "resource": "purchasing-manager",' +
        '    "globalResourcePermissions": [],' +
        '    "companyResourcePermissions": [' +
        '        {' +
        '            "companyNumber": "00110",' +
        '            "permissions": [' +
        '                "purchasing.template.create",' +
        '                "purchasing.template.delete",' +
        '                "purchasing.template.read"' +
        '            ]' +
        '        }' +
        '    ]' +
        '    [   {' +
        '            "companyNumber": "00110",' +
        '            "permissions": [' +
        '                "purchasing.template.create",' +
        '                "purchasing.template.delete",' +
        '                "purchasing.template.read"' +
        '            ]' +
        '        }' +
        '    ]' +
        '}';

    f(theJson);
}

/*
 * GET Handlers
 */
permissionsRouter.route('/:resourceName/users/:username')
    .get(function(req,res) {
        console.log("entering resource permissions route without company number");

        // Fix CORS stuff
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

        const resourceName = req.params.resourceName;
        const user = req.params.username;

        console.log("Handling GET with -" + resourceName + "- and -" + user + "-")

        if (user === 'lazybones')
            res.send('');
        else if (user === 'confused')
            res.sendStatus(503);
        else getStdMultiPermissions(function (permissions) {
            if (typeof permissions != 'undefined') {
                res.send(permissions);
            }
            else res.send("no permission");
        });
    });

permissionsRouter.route('/:resourceName/users/:username/companies/:companyNumber')
    .get(function(req,res) {
        console.log("entering resource permissions route with company number");

        // Fix CORS stuff
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

        const resourceName = req.params.resourceName;
        const user = req.params.username;

        console.log("Handling GET with -" + resourceName + "- and -" + user + "-")

        getStdSinglePermissions(function (permissions) {

            if (typeof permissions != 'undefined') {
                res.send(permissions);
            }
            else res.send("no permission");
        });
    });

module.exports = permissionsRouter;
